CREATE TABLE pessoas (
    cpf numeric(11) PRIMARY KEY,
    nome varchar(50) NOT NULL,  
    telefone varchar(15), 
    data_nasc date
);

CREATE TABLE bebidas (
    codigo serial PRIMARY KEY, 
	nome varchar(35) NOT NULL,
    preco numeric(10)  NOT NULL,
    atual boolean default true
);

CREATE TABLE festas (
    codigo integer PRIMARY KEY,
    nome varchar(50) NOT NULL,
    valor_fem numeric(10), 
    valor_masc numeric(10), 
    inicio timestamp, 
    fim timestamp, 
    estilo_musical varchar(25), 
    tema varchar(50)
);

CREATE TABLE entradas (
    festa_codigo integer, 
    pessoa_cpf numeric(11)
);

CREATE TABLE bebidas_festas (
    bebida_codigo integer NOT NULL, 
    festa_codigo integer NOT NULL, 
    quantidade integer
);

CREATE TABLE funcionarios (
    salario numeric(12), 
    data_cont date, 
    funcao varchar(30))
INHERITS (pessoas)
WITH (
OIDS=FALSE
);

CREATE TABLE clientes (
    masc boolean NOT NULL)
INHERITS (pessoas)
WITH (
OIDS=FALSE
);

ALTER TABLE entradas ADD CONSTRAINT PK_entrada
PRIMARY KEY (festa_codigo, pessoa_cpf);
 
ALTER TABLE entradas ADD CONSTRAINT FK_entrada_festa
FOREIGN KEY (festa_codigo) 
REFERENCES festas (codigo);
 
ALTER TABLE entradas ADD CONSTRAINT FK_entrada_pessoa
FOREIGN KEY (pessoa_cpf) 
REFERENCES pessoas (cpf);

ALTER TABLE bebidas_festas ADD CONSTRAINT PK_bebida_festa
PRIMARY KEY (bebida_codigo, festa_codigo);
 
ALTER TABLE bebidas_festas ADD CONSTRAINT FK_bebida_festa_bebida
FOREIGN KEY (bebida_codigo) 
REFERENCES bebidas (codigo);
 
ALTER TABLE bebidas_festas ADD CONSTRAINT FK_bebida_festa_festa
FOREIGN KEY (festa_codigo) 
REFERENCES festas (codigo);

/*insert em pessoa é proibido 
porque pessoa deve ser especificada como funcionario ou cliente*/
CREATE OR REPLACE FUNCTION insert_proibido_pessoa () RETURNS integer
AS $$
	BEGIN
		raise exception 'Impossivel inserir na tabela pessoa. Pessoa deve ser especializada em funcionario ou cliente.';
	END
$$ LANGUAGE plpgsql;
 
CREATE OR REPLACE RULE rule_insert_pessoa AS
ON INSERT TO pessoas
DO INSTEAD
select insert_proibido_pessoa();

/*insert em bebida
cuida que quando uma bebida seja inserida 
caso ela ja exista, a antiga nao sera mais setada como atual*/
CREATE OR REPLACE FUNCTION insert_bebida() RETURNS trigger 
AS $$
	DECLARE
    	nome_in varchar(35);
    BEGIN
    	nome_in := NEW.nome;
    	UPDATE bebidas SET atual = false where nome = nome_in AND atual = true;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER insert_bebida BEFORE INSERT ON bebidas
FOR EACH ROW EXECUTE PROCEDURE insert_bebida();

/* -------------- testes -------------------
select * from pessoa
select * from cliente
select * from bebida
insert into pessoa values 
('rafael', 07533333314, '48984379808', '1996/07/10')

insert into cliente (nome, cpf, telefone, data_nasc, masc) values 
('rafael', 07522233314, '48984379808', '1996/07/10', true)

insert into bebida (nome, preco) values
('caipirinha', 12),
('vodka', 250),
('cerveja', 5)

update bebida 
set preco = 28
where nome = 'vodka'

delete from bebida
where nome = 'caipirinha'

delete from pessoa

select * from bebidas
delete from bebidas
drop function insert_bebida()
drop trigger insert_bebida on bebida
drop rule update_preco_bebida on bebida
drop function update_preco_bebida()

insert into bebidas (nome, preco) values 
('vodka', 120),
('cerveja', 10),
('caipirinha', 14)

update bebidas
set preco = 444
where nome = 'vodka'

update bebidas
set preco = 12
where nome = 'caipirinha'
*/
